//
//  ShowImageController.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 5/12/17.
//  Copyright © 2017 Cristian Cosneanu. All rights reserved.
//

import UIKit

class ShowImageController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var imageFromSegue: UIImageView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var cropSquare: UIView!
    
    public var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollview.delegate = self
        scrollview.minimumZoomScale = 1.0
        scrollview.maximumZoomScale = 30.0
        self.automaticallyAdjustsScrollViewInsets = false
        if (image != nil)
        {
            imageFromSegue.image = image
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageFromSegue
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editImageFromGoogle"
        {
            if let dest = segue.destination as? SecondViewController
            {
                let imageoff = self.captureScreen()
                let size = CGSize(width: 375, height: 375)
                UIGraphicsBeginImageContext(size)
                let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
                imageoff!.draw(in: areaSize)
                dest.viewImageVar = imageoff
            }
        }
    }
    
    func captureScreen() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.cropSquare.bounds.size, true, UIScreen.main.scale)
        let offset:CGPoint = self.scrollview.contentOffset;
        UIGraphicsGetCurrentContext()!.translateBy(x: -offset.x, y: -offset.y);
        self.scrollview.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }

    @IBAction func editImageAction(_ sender: Any) {
        performSegue(withIdentifier: "editImageFromGoogle", sender: self)
    }
}
