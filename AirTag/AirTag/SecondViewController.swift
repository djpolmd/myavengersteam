//
//  SecondViewController.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 5/8/17.

import UIKit


struct myColorType {
    let red, green, blue: CGFloat
    
    init(_ red:CGFloat, _ green:CGFloat, _ blue:CGFloat) {
        self.red = red / 255.0
        self.green = green / 255.0
        self.blue = blue / 255.0
    }
}



class SecondViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var inverseBTN: UIButton!
    @IBOutlet weak var lockInverseImg: UIImageView!
    
    var inverseStr = "masksinverted"
    let allImagesArr = ["1-FREE", "2-FREE", "animals", "cartoons", "heroes", "invaders", "people", "symbol"]
    let allImageNum = [14, 30, 34, 13, 24, 19, 31, 27]
    var categoriesImages = [UIImage]()
    var imagesByCategory = [UIImage]()
    var picker = UIImagePickerController()
    var viewImageVar: UIImage? = nil
    var videoURL: NSURL? = nil


    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var imagesView: UIView!
    @IBOutlet weak var currentCategoryBTN: UIButton!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var colorCollectionView: UICollectionView!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var scrollView:               UIScrollView!
    @IBOutlet weak var viewImage:                UIImageView!
    @IBOutlet weak var sliderView:               UIView!
    @IBOutlet weak var menuStackTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuView:            UIView!
    @IBOutlet weak var inverseImageForBTN:  UIImageView!
    @IBOutlet weak var sliderBAR:           UISlider!
    
    
    @IBOutlet weak var topImage: UIImageView!
    var sizeVal:Float?
    var opacityVal:Float? 

    
    let logo = UIImage(named:"airshape_logo")
    var long = UILongPressGestureRecognizer()
    var pan = UIPanGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
        self.lockInverseImg.isHidden = true
        self.colorCollectionView.delegate = self
        self.colorCollectionView.dataSource = self
        self.categoriesCollectionView.delegate = self
        self.categoriesCollectionView.dataSource = self
        self.imagesCollectionView.delegate = self
        self.imagesCollectionView.dataSource = self
        self.menuStackTopConstraint.constant = -15
        self.sliderView.isHidden = true
        self.opacityVal = 0.5
        self.sizeVal = 0.5
        let imageView = UIImageView(image: logo)
        let bannerWidth = navigationController?.navigationBar.frame.size.width
        let bannerHeight = navigationController?.navigationBar.frame.size.height
        imageView.frame = CGRect(x: 0, y: 0, width: bannerWidth!, height: bannerHeight!)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 10.0
        self.scrollView.alwaysBounceVertical = true
        self.scrollView.alwaysBounceHorizontal = true
        scrollView.delegate = self
        self.curentCategoryIndex = -1
        
        self.long = UILongPressGestureRecognizer(target: self, action: #selector(self.longRecogniser(_:)))
        self.pan = UIPanGestureRecognizer(target: self, action: #selector(self.panRecogniser(_:)))
        self.long.delegate = self
        self.pan.delegate = self
        self.view.addGestureRecognizer(long)
   
        if (self.viewImageVar != nil)
        {
            self.webView.isHidden = true
            self.viewImage.image = viewImageVar
            self.topImage.image = nil
            self.viewImage.clipsToBounds = true
            self.scrollView.addSubview(self.viewImage)
            
//  self.scrollView.addSubview(self.topImage)
        }
        else if self.videoURL != nil
        {
            self.webView.isHidden = false
            
            self.webView.loadHTMLString("<style>* {margin:0;padding:0;} </style><iframe style=\"border:none\" width = \"\(self.webView.frame.width)\" height = \"\(self.webView.frame.height)\" src=\"\(self.videoURL!)\" > </iframe>", baseURL: nil)
            print("\n\n\n\n", self.videoURL!, "\n\n\n")
            self.scrollView.isHidden = true
        }
        self.topImage.image = self.topImage.image?.maskWithColor(color: UIColor.white)
        self.topImage.layer.opacity = 0.5
        self.colorCollectionView.isHidden = true
        self.imagesView.isHidden = true
        initColors()
        initCategories()
    }
    
    var startedLONG = false // ==========================================<<<<<<<<<<<<<< GESTURES
    
    func longRecogniser(_ sender: UITapGestureRecognizer)
    {
        self.startedLONG = true
        self.topImage.shake(duration: CFTimeInterval(1))
        self.topImage.layer.borderColor = UIColor.red.cgColor
        self.topImage.layer.borderWidth = 2
        self.scrollView.addGestureRecognizer(pan)
        print("taped twice")
    }
   
    func panRecogniser(_ sender: UIPanGestureRecognizer)
    {
        if startedLONG {
            let location = sender.location(in: self.view)
            self.topImage.frame.origin.x = location.x - (self.topImage.frame.width / 2)
            self.topImage.frame.origin.y = location.y - (self.topImage.frame.height / 2)
            print("after tap PAN")
        }
        if sender.state == .ended
        {
            self.startedLONG = false
            self.topImage.layer.borderColor = nil
            self.topImage.layer.borderWidth = 0
            self.scrollView.removeGestureRecognizer(pan)
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return (gestureRecognizer == long && gestureRecognizer == pan)
    }
    
    @IBAction func closeCurentCategoryAction(_ sender: Any) {
        self.imagesView.isHidden = true
    }
    
    func initCategories()
    {
        self.categoriesImages.append(UIImage(named: "material/zIconitelecategoriilor/addImage.png")!)
        var i = 1
        while i <= 8  {
            self.categoriesImages.append(UIImage(named: "material/zIconitelecategoriilor/image-\(i)")!)
            i += 1
        }
        self.categoriesCollectionView.reloadData()
    }
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.viewImage
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
// Dispose of any resources that can be recreated.
    }
    
    var isOpBTN:Bool = false
    var isSiBTN:Bool = false
    
    func toogleSliderOpacity()
    {
        self.sliderBAR.value = self.opacityVal!
        
        isSiBTN = false
        isOpBTN = true
        self.sliderView.isHidden = false
        self.menuStackTopConstraint.constant = 20
    }
    
    func toogleSliderSize()
    {
        self.sliderBAR.value = self.sizeVal!
        
        isOpBTN = false
        isSiBTN = true
        self.sliderView.isHidden = false
        self.menuStackTopConstraint.constant = 20

    }
    
    func toogleColorPanel()
    {
        self.colorCollectionView.isHidden = !self.colorCollectionView.isHidden
    }
    
    func closeSlider()
    {
        isSiBTN = false
        isOpBTN = false
        self.sliderView.isHidden = true
        self.menuStackTopConstraint.constant = -15
    }
    
    var inversed = false
    var curentCategoryIndex:Int = 0
    var curentImageindex:Int = 0
    
    @IBAction func inverseBTN(_ sender: Any) {
        print("inverseBTN btn taped")
        inversed = !inversed
        if inversed
        {
            self.inverseStr = "material"
            self.inverseImageForBTN.image = UIImage(named: "icon_invert_inverted")
        }
        else
        {
            self.inverseStr = "masksinverted"
            self.inverseImageForBTN.image = UIImage(named: "inverse")
        }
        if self.curentCategoryIndex != -1
        {
            self.setImagesByCategory(index: self.curentCategoryIndex - 1)
            self.imagesCollectionView.reloadData()
            self.topImage.image = UIImage(named: "\(self.inverseStr)/\(self.allImagesArr[self.curentCategoryIndex - 1])/image-\(self.curentImageindex)")
            self.topImage.image = self.topImage.image?.maskWithColor(color: self.curentColor)
            self.setImagesByCategory(index: self.curentCategoryIndex - 1)
        }
    }

    @IBAction func sizeBTN(_ sender: Any) {
        print("sizeBTN btn taped")
        toogleSliderSize()
    }
    
    @IBAction func opacityBTN(_ sender: Any) {
        print("opacityBTN btn taped")
        toogleSliderOpacity()
    }
    
    @IBAction func colorBTN(_ sender: Any) {
        print("colorBTN is taped")
        toogleColorPanel()
    }
    
    @IBAction func sliderCloseBTN(_ sender: Any) {
        closeSlider()
    }
    
    @IBAction func sliderSaveBTN(_ sender: Any) {
        closeSlider()
    }
    
    @IBAction func sliderAction(_ sender: Any) {
        if isSiBTN
        {
            self.sizeVal = self.sliderBAR.value
            self.topImage.transform = CGAffineTransform(scaleX: CGFloat(sizeVal!), y: CGFloat(sizeVal!))

        }
        else if isOpBTN
        {
            self.opacityVal = self.sliderBAR.value
            self.topImage.layer.opacity = self.opacityVal!
            
        }
    }
    
    @IBAction func saveGallery(_ sender: UIBarButtonItem) {
        let scrollviewsize = self.scrollView.frame.width
        let widthx: CGFloat = scrollviewsize * topImage.transform.a
        let highty: CGFloat = scrollviewsize * topImage.transform.d
        let imageoff = captureScreen()
        let size = CGSize(width: scrollviewsize, height: scrollviewsize)
        UIGraphicsBeginImageContext(size)
        let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        imageoff!.draw(in: areaSize)
        let topareaSize = CGRect(x: topImage.frame.origin.x , y: topImage.frame.origin.y, width: widthx, height: highty)
        topImage.image?.draw(in: topareaSize, blendMode: CGBlendMode.normal, alpha: CGFloat(self.opacityVal!))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let imageToShare = [ newImage ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        //activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        //UIImageWriteToSavedPhotosAlbum(newImage, self,  #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error == nil {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        }
    }
    
    func captureScreen() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.scrollView.bounds.size, true, UIScreen.main.scale)
        let offset:CGPoint = self.scrollView.contentOffset;
        UIGraphicsGetCurrentContext()!.translateBy(x: -offset.x, y: -offset.y);
        self.scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == colorCollectionView
        {
            return self.colors.count
        }
        else if collectionView == categoriesCollectionView
        {
            return self.categoriesImages.count
        }
        else if collectionView == imagesCollectionView
        {
            return self.imagesByCategory.count
        }
        return 1
    }
    
    func setImagesByCategory(index: Int)
    {
        self.imagesByCategory.removeAll()
        var i = 1
        while i <= self.allImageNum[index]
        {
            self.imagesByCategory.append(UIImage(named: "\(self.inverseStr)/\(self.allImagesArr[index])/image-\(i)")!)
            i += 1
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            self.inverseBTN.isEnabled = false
            self.inverseBTN.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.01)
            self.lockInverseImg.isHidden = false
            self.topImage.image = image
        }
        else
        {
            print("error")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == colorCollectionView
        {
            let cell = collectionView.cellForItem(at: indexPath) as! ColorCollectionViewCell
            self.curentColor = cell.CellColor!
            self.topImage.image = self.topImage.image?.maskWithColor(color: cell.CellColor!)
        }
        else if collectionView == categoriesCollectionView
        {
            if indexPath.row > 0
            {
                setImagesByCategory(index: indexPath.row - 1)
                self.imagesView.isHidden = false
                self.currentCategoryBTN.setImage(self.categoriesImages[indexPath.row], for: UIControlState.normal)
                curentCategoryIndex = indexPath.row
                self.imagesCollectionView.reloadData()
            }
            else
            {
                // chose shape from gallery
                self.picker.allowsEditing = false //2
                self.picker.sourceType = .photoLibrary
                present(picker, animated: true, completion: nil)//4
            }
        }
        else if collectionView == imagesCollectionView
        {
            self.inverseBTN.isEnabled = true
            self.inverseBTN.backgroundColor = UIColor(red: 254, green: 209, blue: 22, alpha: 0)
            self.lockInverseImg.isHidden = true
            self.topImage.image = self.imagesByCategory[indexPath.row]
            self.curentImageindex = indexPath.row + 1
            self.topImage.image = self.topImage.image?.maskWithColor(color: self.curentColor)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == categoriesCollectionView
        {
            let imageCell:ImageCategoryCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCategoryCell", for: indexPath) as! ImageCategoryCollectionViewCell
            imageCell.image.image = self.categoriesImages[indexPath.row]
            return imageCell
        }
        else if collectionView == colorCollectionView
        {
            let cell:ColorCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorCell", for: indexPath) as! ColorCollectionViewCell
            let col = self.colors[indexPath.row]
            cell.backgroundColor = UIColor(red: col.red, green: col.green, blue: col.blue, alpha: 1)
            cell.CellColor = UIColor(red: col.red, green: col.green, blue: col.blue, alpha: 1)
            return cell
        }
        else if collectionView == imagesCollectionView
        {
            let cell:imageViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! imageViewCell
            cell.image.image = self.imagesByCategory[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func initColors()
    {
        self.colors = [
            myColorType(240, 248, 255),
            myColorType(227, 38, 54),
            myColorType(229, 43, 80),
            myColorType(255, 191, 0),
            myColorType(153, 102, 204),
            myColorType(251, 206, 177),
            myColorType(0, 255, 255),
            myColorType(127, 255, 212),
            myColorType(75, 83, 32),
            myColorType(123, 160, 91),
            myColorType(255, 153, 102),
            myColorType(111, 53, 26),
            myColorType(0, 127, 255),
            myColorType(240, 255, 255),
            myColorType(224, 255, 255),
            myColorType(245, 245, 220),
            myColorType(61, 43, 31),
            myColorType(0, 0, 0),
            myColorType(0, 0, 255),
            myColorType(51, 51, 153),
            myColorType(2, 71, 254),
            myColorType(0, 223, 223),
            myColorType(138, 43, 226),
            myColorType(0, 149, 182),
            myColorType(181, 166, 66),
            myColorType(102, 255, 0),
            myColorType(255, 0, 127),
            myColorType(8, 232, 222),
            myColorType(255, 85, 163),
            myColorType(0, 66, 37),
            myColorType(205, 127, 50),
            myColorType(150, 75, 0),
            myColorType(240, 220, 130),
            myColorType(128, 0, 32),
            myColorType(204, 85, 0),
            myColorType(233, 116, 81),
            myColorType(138, 51, 36),
            myColorType(120, 134, 107),
            myColorType(89, 39, 32),
            myColorType(196, 30, 58),
            myColorType(150, 0, 24),
            myColorType(255, 166, 201),
            myColorType(156, 186, 227),
            myColorType(237, 145, 33),
            myColorType(172, 225, 175),
            myColorType(222, 49, 99),
            myColorType(0, 123, 167),
            myColorType(42, 82, 190),
            myColorType(247, 231, 206),
            myColorType(70, 70, 70),
            myColorType(223, 255, 0),
            myColorType(127, 255, 0),
            myColorType(255, 183, 197),
            myColorType(205, 92, 92),
            myColorType(123, 63, 0),
            myColorType(227, 66, 52),
            myColorType(210, 105, 30),
            myColorType(0, 71, 171),
            myColorType(155, 221, 255),
            myColorType(184, 115, 51),
            myColorType(153, 102, 102),
            myColorType(255, 127, 80),
            myColorType(255, 64, 64),
            myColorType(251, 236, 93),
            myColorType(100, 149, 237),
            myColorType(255, 248, 231),
            myColorType(255, 253, 208),
            myColorType(220, 20, 60),
            myColorType(0, 255, 255),
            myColorType(0, 180, 247),
            myColorType(0, 0, 139),
            myColorType(101, 67, 33),
            myColorType(8, 69, 126),
            myColorType(152, 105, 96),
            myColorType(205, 91, 69),
            myColorType(184, 134, 11),
            myColorType(1, 50, 32),
            myColorType(189, 183, 107),
            myColorType(3, 192, 60),
            myColorType(231, 84, 128),
            myColorType(86, 3, 125),
            myColorType(233, 150, 122),
            myColorType(47, 79, 79),
            myColorType(23, 114, 69),
            myColorType(145, 129, 81),
            myColorType(0, 206, 209),
            myColorType(148, 0, 211),
            myColorType(218, 50, 135),
            myColorType(185, 78, 72),
            myColorType(193, 84, 193),
            myColorType(153, 85, 187),
            myColorType(204, 0, 204),
            myColorType(255, 203, 164),
            myColorType(255, 20, 147),
            myColorType(21, 96, 189),
            myColorType(30, 144, 255),
            myColorType(194, 178, 128),
            myColorType(16, 52, 166),
            myColorType(125, 249, 255),
            myColorType(0, 255, 0),
            myColorType(102, 0, 255),
            myColorType(204, 255, 0),
            myColorType(191, 0, 255),
            myColorType(80, 200, 120),
            myColorType(97, 64, 81),
            myColorType(128, 24, 24),
            myColorType(79, 121, 66),
            myColorType(178, 34, 34),
            myColorType(238, 220, 130),
            myColorType(34, 139, 34),
            myColorType(246, 74, 138),
            myColorType(255, 0, 255),
            myColorType(255, 119, 255),
            myColorType(228, 155, 15),
            myColorType(212, 175, 55),
            myColorType(255, 215, 0),
            myColorType(153, 101, 21),
            myColorType(255, 223, 0),
            myColorType(218, 165, 32),
            myColorType(70, 89, 69),
            myColorType(0, 255, 0),
            myColorType(0, 128, 0),
            myColorType(0, 165, 80),
            myColorType(102, 176, 50),
            myColorType(173, 255, 47),
            myColorType(128, 128, 128),
            myColorType(82, 24, 250),
            myColorType(63, 255, 0),
            myColorType(223, 115, 255),
            myColorType(244, 0, 161),
            myColorType(255, 0, 204),
            myColorType(255, 105, 180),
            myColorType(0, 65, 106),
            myColorType(75, 0, 130),
            myColorType(0, 47, 167),
            myColorType(255, 79, 0),
            myColorType(0, 153, 0),
            myColorType(255, 255, 240),
            myColorType(0, 168, 107),
            myColorType(76, 187, 23),
            myColorType(195, 176, 145),
            myColorType(240, 230, 140),
            myColorType(181, 126, 220),
            myColorType(230, 230, 250),
            myColorType(204, 204, 255),
            myColorType(255, 240, 245),
            myColorType(196, 195, 221),
            myColorType(238, 130, 238),
            myColorType(251, 174, 210),
            myColorType(150, 120, 182),
            myColorType(251, 160, 227),
            myColorType(124, 252, 0),
            myColorType(253, 233, 16),
            myColorType(255, 250, 205),
            myColorType(173, 216, 230),
            myColorType(255, 182, 193),
            myColorType(200, 162, 200),
            myColorType(191, 255, 0),
            myColorType(0, 255, 0),
            myColorType(50, 205, 50),
            myColorType(250, 240, 230),
            myColorType(255, 0, 255),
            myColorType(202, 31, 23),
            myColorType(255, 0, 144),
            myColorType(170, 240, 209),
            myColorType(248, 244, 255),
            myColorType(11, 218, 81),
            myColorType(128, 0, 0),
            myColorType(176, 48, 96),
            myColorType(115, 194, 251),
            myColorType(224, 176, 255),
            myColorType(145, 95, 109),
            myColorType(0, 0, 205),
            myColorType(175, 64, 53),
            myColorType(204, 153, 204),
            myColorType(147, 112, 219),
            myColorType(0, 250, 154),
            myColorType(0, 51, 102),
            myColorType(152, 255, 152),
            myColorType(255, 228, 225),
            myColorType(173, 223, 173),
            myColorType(153, 122, 141),
            myColorType(255, 219, 88),
            myColorType(33, 66, 30),
            myColorType(255, 222, 173),
            myColorType(0, 0, 128),
            myColorType(204, 119, 34),
            myColorType(0, 128, 0),
            myColorType(207, 181, 59),
            myColorType(253, 245, 230),
            myColorType(121, 104, 120),
            myColorType(192, 46, 76),
            myColorType(128, 128, 0),
            myColorType(107, 142, 35),
            myColorType(154, 185, 115),
            myColorType(255, 127, 0),
            myColorType(251, 153, 2),
            myColorType(255, 165, 0),
            myColorType(255, 160, 0),
            myColorType(255, 69, 0),
            myColorType(218, 112, 214),
            myColorType(175, 238, 238),
            myColorType(152, 118, 84),
            myColorType(175, 64, 53),
            myColorType(221, 173, 175),
            myColorType(171, 205, 239),
            myColorType(249, 132, 229),
            myColorType(250, 218, 221),
            myColorType(219, 112, 147),
            myColorType(255, 239, 213),
            myColorType(119, 221, 119),
            myColorType(255, 209, 220),
            myColorType(255, 229, 180),
            myColorType(255, 204, 153),
            myColorType(250, 223, 173),
            myColorType(209, 226, 49),
            myColorType(204, 204, 255),
            myColorType(28, 57, 187),
            myColorType(0, 166, 147),
            myColorType(50, 18, 122),
            myColorType(217, 144, 88),
            myColorType(204, 51, 51),
            myColorType(247, 127, 190),
            myColorType(254, 40, 162),
            myColorType(236, 88, 0),
            myColorType(1, 121, 111),
            myColorType(255, 192, 203),
            myColorType(255, 153, 102),
            myColorType(229, 228, 226),
            myColorType(204, 153, 204),
            myColorType(176, 224, 230),
            myColorType(204, 136, 153),
            myColorType(0, 49, 83),
            myColorType(221, 0, 255),
            myColorType(255, 117, 24),
            myColorType(128, 0, 128),
            myColorType(160, 92, 240),
            myColorType(80, 64, 77),
            myColorType(115, 74, 18),
            myColorType(227, 11, 92),
            myColorType(255, 0, 0),
            myColorType(237, 28, 36),
            myColorType(254, 39, 18),
            myColorType(199, 21, 133),
            myColorType(215, 0, 64),
            myColorType(0, 204, 204),
            myColorType(255, 0, 127),
            myColorType(227, 38, 54),
            myColorType(144, 93, 93),
            myColorType(65, 105, 225),
            myColorType(107, 63, 160),
            myColorType(224, 17, 95),
            myColorType(128, 70, 27),
            myColorType(183, 65, 14),
            myColorType(255, 102, 0),
            myColorType(255, 102, 0),
            myColorType(244, 196, 48),
            myColorType(255, 140, 105),
            myColorType(244, 164, 96),
            myColorType(146, 0, 10),
            myColorType(8, 37, 103),
            myColorType(255, 36, 0),
            myColorType(255, 216, 0),
            myColorType(46, 139, 87),
            myColorType(255, 245, 238),
            myColorType(255, 186, 0),
            myColorType(112, 66, 20),
            myColorType(0, 158, 96),
            myColorType(252, 15, 192),
            myColorType(192, 192, 192),
            myColorType(135, 206, 235),
            myColorType(112, 128, 144),
            myColorType(0, 51, 153),
            myColorType(167, 252, 0),
            myColorType(0, 255, 127),
            myColorType(70, 130, 180),
            myColorType(210, 180, 140),
            myColorType(242, 133, 0),
            myColorType(255, 204, 0),
            myColorType(72, 60, 50),
            myColorType(208, 240, 192),
            myColorType(248, 131, 194),
            myColorType(244, 194, 194),
            myColorType(0, 128, 128),
            myColorType(205, 87, 0),
            myColorType(226, 114, 91),
            myColorType(216, 191, 216),
            myColorType(255, 99, 71),
            myColorType(48, 213, 200),
            myColorType(72, 145, 206),
            myColorType(102, 2, 60),
            myColorType(18, 10, 143),
            myColorType(91, 146, 229),
            myColorType(197, 179, 88),
            myColorType(227, 66, 51),
            myColorType(139, 0, 255),
            myColorType(238, 130, 238),
            myColorType(2, 71, 54),
            myColorType(64, 130, 109),
            myColorType(245, 222, 179),
            myColorType(255, 255, 255),
            myColorType(201, 160, 220),
            myColorType(255, 255, 0),
            myColorType(255, 239, 0),
            myColorType(254, 254, 51),
            myColorType(154, 205, 50),
            myColorType(235, 194, 175)
        ]
    }
}

extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
}

extension UIImageView
{
    func shake(duration: CFTimeInterval) {
        let translation = CAKeyframeAnimation(keyPath: "transform.translation.x");
        translation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        translation.values = [-5, 5, -5, 5, -5, 5, -5, 5, -5, 5, -5, 5, -3, 3, -2, 2, 0]
        
        let rotation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        rotation.values = [-5, 5, -5, 5, -5, 5, -5, 5, -5, 5, -5, 5, -3, 3, -2, 2, 0].map {
            ( degrees: Double) -> Double in
            let radians: Double = (Double.pi * degrees) / 180.0
            return radians
        }
        let shakeGroup: CAAnimationGroup = CAAnimationGroup()
        shakeGroup.animations = [translation, rotation]
        shakeGroup.duration = duration
        self.layer.add(shakeGroup, forKey: "shakeIt")
    }
}
