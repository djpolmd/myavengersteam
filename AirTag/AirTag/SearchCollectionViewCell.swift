//
//  SearchCollectionViewCell.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 5/12/17.
//  Copyright © 2017 Cristian Cosneanu. All rights reserved.
//

import UIKit

class SearchCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
}
