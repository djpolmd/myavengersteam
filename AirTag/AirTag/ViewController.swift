//
//  ViewController.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 5/6/17.
//  Copyright � 2017 Cristian Cosneanu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    
    @IBOutlet weak var animationImage: UIImageView!
    
    @IBOutlet weak var myImageView: UIImageView!
    
    let picker = UIImagePickerController()
    
    let images = ["animation_image_1","animation_image_2","animation_image_3","animation_image_4","animation_image_5"]
    var animImages = [UIImage]()
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage 
        myImageView.contentMode = .scaleAspectFit //3
        myImageView.image = chosenImage //4
        dismiss(animated: true, completion: nil) //5
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ViewController.
        picker.delegate = self
    
        for i in 0..<self.images.count
        {
            self.animImages.append(UIImage(named: self.images[i])!)
        }
        
        self.animationImage.animationImages = self.animImages
        self.animationImage.animationDuration = 1
        self.animationImage.startAnimating()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func photoFromLibrary(_ sender: UIButton) {
        picker.allowsEditing = true
        picker.mediaTypes = ["public.image", "public.movie"]
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }

    @IBAction func openCamera(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self

        image.sourceType = UIImagePickerControllerSourceType.camera
        
        image.allowsEditing = false
        
        self.present(image, animated:  true)
        {
            
        }
    }
    
    var selectedImage: UIImage? = nil
    var selectedVideo: NSURL? = nil
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "secondViewSegue"
        {
            if let dest = segue.destination as? SecondViewController
            {
                if self.selectedImage != nil
                {
                    dest.viewImageVar = self.selectedImage
                    dest.videoURL = nil
                }
                else if self.selectedVideo != nil
                {
                    dest.videoURL = self.selectedVideo
                    dest.viewImageVar = nil
                }
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if info[UIImagePickerControllerMediaType] as! String == "public.movie"
        {
            let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL
            self.selectedVideo = videoURL
            self.selectedImage = nil
            performSegue(withIdentifier: "secondViewSegue", sender: self)
        }
        else if let image = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            self.selectedImage = image
            self.selectedVideo = nil
            performSegue(withIdentifier: "secondViewSegue", sender: self)
        }
        else
        {
            print("error")
        }
        self.dismiss(animated: true, completion: nil)
    }

}


